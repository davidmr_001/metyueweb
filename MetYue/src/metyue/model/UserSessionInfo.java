package metyue.model;

public class UserSessionInfo implements java.io.Serializable{

	private static final long serialVersionUID = -2505914320862168466L;
	
	private Long userPtr;
	private String manageLevel;

	public UserSessionInfo() {
		super();
	}

	public UserSessionInfo(Long userPtr) {
		super();
		this.userPtr = userPtr;
	}

	public UserSessionInfo(Long userPtr, String manageLevel) {
		super();
		this.userPtr = userPtr;
		this.manageLevel = manageLevel;
	}

	public Long getUserPtr() {
		return userPtr;
	}

	public void setUserPtr(Long userPtr) {
		this.userPtr = userPtr;
	}

	public String getManageLevel() {
		return manageLevel;
	}

	public void setManageLevel(String manageLevel) {
		this.manageLevel = manageLevel;
	}
	
	
	
}
