package metyue.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import metyue.model.ActionResult;
import metyue.model.MemberCenterVO;
import metyue.model.UserOptLogo;
import metyue.model.UserSessionInfo;
import metyue.service.ActionResultService;
import metyue.service.ImprintService;
import metyue.service.MemberCenterService;
import metyue.utils.WebUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.WebUtils;

@Controller
@RequestMapping(value = "/member")
public class MemberCenterAction {

	@Autowired
	private MemberCenterService memberCenterService;
	
	@Autowired
	private ActionResultService actionResultService;
	
	@Autowired
	private ImprintService imprintService;
	
	//Trun on fans
	@RequestMapping(value = "/trunOnFans")
	public void trunOnFans(HttpServletRequest request, HttpServletResponse response){
		
		ActionResult objResult = null;
		
		String fansPtr = WebUtils.findParameterValue(request, "FansPtr");
		
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{				
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				long userPtr = (long)session.getUserPtr();
				
				if(Long.valueOf(userPtr) != Long.valueOf(fansPtr)){
				
					memberCenterService.trunOnFans(Long.valueOf(userPtr), Long.valueOf(fansPtr));
					
					//保存系统记录
					UserOptLogo userOptLogo = new UserOptLogo(
							userPtr, 
							appSource, 
							WebUtil.MEMBER_TRUN_ON_FANS, 
							"trunOnFans", 
							fansPtr, 
							userPtr, 
							new Date()
						);
					
					imprintService.saveUserOptLogo(userOptLogo);
					
					objResult = actionResultService.getActionResultItem("20021");
				
				}else{
					
					objResult = actionResultService.getActionResultItem("20022");
				}

			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
			}
			
		}catch (Exception e) {			
			
			actionResultService.getActionResultItem("10000");		
			
			e.printStackTrace();
			
		}	
		
		WebUtil.responseToJson( response, objResult ); 
		
	}	

	
	//get member center list 
	@RequestMapping(value = "/getMemberCenterList")
	public void getMemberCenterList(HttpServletRequest request, HttpServletResponse response){
		
		String userPtr = WebUtils.findParameterValue(request, "USER_PTR");
		String keyWords = WebUtil.decode2( WebUtils.findParameterValue(request, "KEY_WORDS") );
		String querySex = WebUtils.findParameterValue(request, "QUERY_SEX");
		String queryPlan = WebUtils.findParameterValue(request, "QUERY_PLAN");
		String queryAge1 = WebUtils.findParameterValue(request, "QUERY_AGE1");
		String queryAge2 = WebUtils.findParameterValue(request, "QUERY_AGE2");
		String optFans = WebUtils.findParameterValue(request, "OPT_FANS");
		String optFocuer = WebUtils.findParameterValue(request, "OPT_FOCUSER");
		String optOnline = WebUtils.findParameterValue(request, "OPT_ONLINE");
		String optHaveContact = WebUtils.findParameterValue(request, "OPT_HAVE_CONTACT");
		
		//String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
				
		try{				
						
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			long userCurPtr = -1L;
			if(session != null){	
				userCurPtr = (long)session.getUserPtr();
			}
			
			Map<String, String> para = new HashMap<String,String>();
			para.put("USER_PTR", userPtr);
			para.put("USER_CUR_PTR", String.valueOf(userCurPtr));
			para.put("KEY_WORDS", keyWords);
			para.put("QUERY_SEX", querySex);
			para.put("QUERY_PLAN", queryPlan);
			para.put("QUERY_AGE1", queryAge1);
			para.put("QUERY_AGE2", queryAge2);
			para.put("OPT_FANS", optFans);
			para.put("OPT_FOCUSER", optFocuer);
			para.put("OPT_ONLINE", optOnline);
			para.put("OPT_HAVE_CONTACT", optHaveContact);
			
			List<MemberCenterVO> list = memberCenterService.getMemberCenterList(para);
			
			WebUtil.responseToJson( response, list ); 
			
		}catch (Exception e) {
			
			ActionResult objResult = actionResultService.getActionResultItem("10000");
			WebUtil.responseToJson( response, objResult );			
			
			e.printStackTrace();
			
		}
		
	}
	
	//get member center list 
	@RequestMapping(value = "/getMemberInfo")
	public void getMemberInfo(HttpServletRequest request, HttpServletResponse response){
		
		String userPtr = WebUtils.findParameterValue(request, "USER_PTR");
		
		//String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
				
		try{				
						
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			long userCurPtr = -1L;
			if(session != null){	
				userCurPtr = (long)session.getUserPtr();
			}
			
			Map<String, String> para = new HashMap<String,String>();
			para.put("USER_PTR", userPtr);
			para.put("USER_CUR_PTR", String.valueOf(userCurPtr));
			
			List<MemberCenterVO> list = memberCenterService.getUserPhotoInfoVOByUserPtr(para);
			
			WebUtil.responseToJson( response, list ); 
			
		}catch (Exception e) {
			
			ActionResult objResult = actionResultService.getActionResultItem("10000");
			WebUtil.responseToJson( response, objResult );			
			
			e.printStackTrace();
			
		}
		
	}
}
