package metyue.model;
// Generated 2014-7-26 9:21:38 by Hibernate Tools 3.6.0


import java.util.Date;

/**
 * UserBaseInfo generated by hbm2java
 */
public class UserBaseInfo  implements java.io.Serializable {

	private static final long serialVersionUID = -1081307647174278335L;
	private long userPtr;
     private UserInfo userInfo;
     private Integer age;
     private String currentAddress;
     private Integer height;
     private Integer weight;
     private String marry;
     private String job;
     private String education;
     private Date lastUpdate;
     private long lastUserPtr;     

    public UserBaseInfo() {
    }

	
    public UserBaseInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    } 
    
    public UserBaseInfo(long userPtr, Integer age, String currentAddress,
			Integer height, Integer weight, String marry, String job,
			String education, Date lastUpdate, long lastUserPtr) {
		super();
		this.userPtr = userPtr;
		this.age = age;
		this.currentAddress = currentAddress;
		this.height = height;
		this.weight = weight;
		this.marry = marry;
		this.job = job;
		this.education = education;
		this.lastUpdate = lastUpdate;
		this.lastUserPtr = lastUserPtr;
	}

	public UserBaseInfo(Integer age, String currentAddress, Integer height, Integer weight, String marry, String job, String education, Date lastUpdate, long lastUserPtr) {
        this.age = age;
        this.currentAddress = currentAddress;
        this.height = height;
        this.weight = weight;
        this.marry = marry;
        this.job = job;
        this.education = education;
        this.lastUpdate = lastUpdate;
        this.lastUserPtr = lastUserPtr;
     }
    
    public UserBaseInfo(UserInfo userInfo, Integer age, String currentAddress, Integer height, Integer weight, String marry, String job, String education, Date lastUpdate, long lastUserPtr) {
       this.userInfo = userInfo;
       this.age = age;
       this.currentAddress = currentAddress;
       this.height = height;
       this.weight = weight;
       this.marry = marry;
       this.job = job;
       this.education = education;
       this.lastUpdate = lastUpdate;
       this.lastUserPtr = lastUserPtr;
    }
   
    public long getUserPtr() {
        return this.userPtr;
    }
    
    public void setUserPtr(long userPtr) {
        this.userPtr = userPtr;
    }
    public UserInfo getUserInfo() {
        return this.userInfo;
    }
    
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
    public Integer getAge() {
        return this.age;
    }
    
    public void setAge(Integer age) {
        this.age = age;
    }
    public String getCurrentAddress() {
        return this.currentAddress;
    }
    
    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }
    public Integer getHeight() {
        return this.height;
    }
    
    public void setHeight(Integer height) {
        this.height = height;
    }
    public Integer getWeight() {
        return this.weight;
    }
    
    public void setWeight(Integer weight) {
        this.weight = weight;
    }
    public String getMarry() {
        return this.marry;
    }
    
    public void setMarry(String marry) {
        this.marry = marry;
    }
    public String getJob() {
        return this.job;
    }
    
    public void setJob(String job) {
        this.job = job;
    }
    public String getEducation() {
        return this.education;
    }
    
    public void setEducation(String education) {
        this.education = education;
    }
    public Date getLastUpdate() {
        return this.lastUpdate;
    }
    
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    public long getLastUserPtr() {
        return this.lastUserPtr;
    }
    public void setLastUserPtr(long lastUserPtr) {
        this.lastUserPtr = lastUserPtr;
    }

}


