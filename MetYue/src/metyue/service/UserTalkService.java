package metyue.service;

import java.util.List;

import metyue.dao.UserTalkDao;
import metyue.model.UserTalkInfo;
import metyue.model.UserTalkInfoVO;
import metyue.model.UserTalkPanel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserTalkService {

	@Autowired
	private UserTalkDao userTalkDao;
	
	public void send(UserTalkInfo userTalkInfo){
		
		userTalkDao.send(userTalkInfo);		
	}
	
	public List<UserTalkPanel> getUserTalkPanelList(long receiveUserPtr){
		
		return userTalkDao.getUserTalkPanelList(receiveUserPtr);
	}
	
	public int getNotReadTalkInfoCount(long receiveUserPtr){
		
		return userTalkDao.getNotReadTalkInfoCount( receiveUserPtr);		
	}
	
	public int rest(long sendUserPtr, long receiveUserPtr, long posStart, long posEnd){
		
		return userTalkDao.resetReadded( sendUserPtr,  receiveUserPtr,  posStart,  posEnd);		
	}
	
	public List<UserTalkInfoVO> read(long sendUserPtr, long receiveUserPtr, long pageSize){
		
		return userTalkDao.getTalksList( sendUserPtr, receiveUserPtr, pageSize);
	}
	
	public List<UserTalkInfoVO> news(long sendUserPtr, long receiveUserPtr, long pageSize){
		
		return userTalkDao.getNewTalksList( sendUserPtr, receiveUserPtr, pageSize);
	}
	
}
