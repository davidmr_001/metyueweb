package metyue.model;

public class FilterContent implements java.io.Serializable{

	private static final long serialVersionUID = 2899759857374542020L;

	private Long pkey;
    private String userName;
    private String purposeDesc;    

	public FilterContent() {
		super();
		// TODO Auto-generated constructor stub
	}
    
	public FilterContent(Long pkey, String userName, String purposeDesc) {
		super();
		this.pkey = pkey;
		this.userName = userName;
		this.purposeDesc = purposeDesc;
	}

	public Long getPkey() {
		return pkey;
	}

	public void setPkey(Long pkey) {
		this.pkey = pkey;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPurposeDesc() {
		return purposeDesc;
	}

	public void setPurposeDesc(String purposeDesc) {
		this.purposeDesc = purposeDesc;
	}
	
	
}
