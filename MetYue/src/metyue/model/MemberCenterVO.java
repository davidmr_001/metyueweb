package metyue.model;

import java.io.Serializable;

public class MemberCenterVO implements Serializable{	

	private static final long serialVersionUID = -2529761272722553461L;
	private long userPtr;
    private String userName;
    private String sexDesc;
    private long age;
    private String currentAddress;
    private long height;
    private long weight;
    private String marryDesc;
    private String jobDesc;
    private String eduDesc;
    private String planDesc;   
    private String expressDesc;
    private String mannerDesc;
    private String purposeDesc;
    private String tencentNum;
    private String classTencentNum;
    private String tencentWeixien;
    private String classWeixien;        
    private String sinaWeibo;
    private String classSinaWeibo;
    private String renrenUrl;
    private String classRenren;
    private String telphone;
    private String classTelphone;
    private String othContactBy;
    private String classOthContactBy;
    private String coverImageUrl;
    private String optFans;
    private String optFansDesc;
    private String optFocuser;
    private String optFocuserDesc;
    private String onlineStatusDesc;
    private String isHaveContact;
        
	public MemberCenterVO() {
		super();
	}

	public MemberCenterVO(long userPtr, String userName, String sexDesc,
			long age, String currentAddress, long height, long weight,
			String marryDesc, String jobDesc, String eduDesc, String planDesc,
			String expressDesc, String mannerDesc, String purposeDesc,
			String tencentNum, String classTencentNum, String tencentWeixien,
			String classWeixien, String sinaWeibo, String classSinaWeibo,
			String renrenUrl, String classRenren, String telphone,
			String classTelphone, String othContactBy,
			String classOthContactBy, String coverImageUrl, String optFans,
			String optFansDesc, String optFocuser, String optFocuserDesc,
			String onlineStatusDesc, String isHaveContact) {
		super();
		this.userPtr = userPtr;
		this.userName = userName;
		this.sexDesc = sexDesc;
		this.age = age;
		this.currentAddress = currentAddress;
		this.height = height;
		this.weight = weight;
		this.marryDesc = marryDesc;
		this.jobDesc = jobDesc;
		this.eduDesc = eduDesc;
		this.planDesc = planDesc;
		this.expressDesc = expressDesc;
		this.mannerDesc = mannerDesc;
		this.purposeDesc = purposeDesc;
		this.tencentNum = tencentNum;
		this.classTencentNum = classTencentNum;
		this.tencentWeixien = tencentWeixien;
		this.classWeixien = classWeixien;
		this.sinaWeibo = sinaWeibo;
		this.classSinaWeibo = classSinaWeibo;
		this.renrenUrl = renrenUrl;
		this.classRenren = classRenren;
		this.telphone = telphone;
		this.classTelphone = classTelphone;
		this.othContactBy = othContactBy;
		this.classOthContactBy = classOthContactBy;
		this.coverImageUrl = coverImageUrl;
		this.optFans = optFans;
		this.optFansDesc = optFansDesc;
		this.optFocuser = optFocuser;
		this.optFocuserDesc = optFocuserDesc;
		this.onlineStatusDesc = onlineStatusDesc;
		this.isHaveContact = isHaveContact;
	}

	public long getUserPtr() {
		return userPtr;
	}

	public void setUserPtr(long userPtr) {
		this.userPtr = userPtr;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSexDesc() {
		return sexDesc;
	}

	public void setSexDesc(String sexDesc) {
		this.sexDesc = sexDesc;
	}

	public long getAge() {
		return age;
	}

	public void setAge(long age) {
		this.age = age;
	}

	public String getCurrentAddress() {
		return currentAddress;
	}

	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}

	public long getHeight() {
		return height;
	}

	public void setHeight(long height) {
		this.height = height;
	}

	public long getWeight() {
		return weight;
	}

	public void setWeight(long weight) {
		this.weight = weight;
	}

	public String getMarryDesc() {
		return marryDesc;
	}

	public void setMarryDesc(String marryDesc) {
		this.marryDesc = marryDesc;
	}

	public String getJobDesc() {
		return jobDesc;
	}

	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}

	public String getEduDesc() {
		return eduDesc;
	}

	public void setEduDesc(String eduDesc) {
		this.eduDesc = eduDesc;
	}

	public String getPlanDesc() {
		return planDesc;
	}

	public void setPlanDesc(String planDesc) {
		this.planDesc = planDesc;
	}

	public String getExpressDesc() {
		return expressDesc;
	}

	public void setExpressDesc(String expressDesc) {
		this.expressDesc = expressDesc;
	}

	public String getMannerDesc() {
		return mannerDesc;
	}

	public void setMannerDesc(String mannerDesc) {
		this.mannerDesc = mannerDesc;
	}

	public String getPurposeDesc() {
		return purposeDesc;
	}

	public void setPurposeDesc(String purposeDesc) {
		this.purposeDesc = purposeDesc;
	}

	public String getTencentNum() {
		return tencentNum;
	}

	public void setTencentNum(String tencentNum) {
		this.tencentNum = tencentNum;
	}

	public String getClassTencentNum() {
		return classTencentNum;
	}

	public void setClassTencentNum(String classTencentNum) {
		this.classTencentNum = classTencentNum;
	}

	public String getTencentWeixien() {
		return tencentWeixien;
	}

	public void setTencentWeixien(String tencentWeixien) {
		this.tencentWeixien = tencentWeixien;
	}

	public String getClassWeixien() {
		return classWeixien;
	}

	public void setClassWeixien(String classWeixien) {
		this.classWeixien = classWeixien;
	}

	public String getSinaWeibo() {
		return sinaWeibo;
	}

	public void setSinaWeibo(String sinaWeibo) {
		this.sinaWeibo = sinaWeibo;
	}

	public String getClassSinaWeibo() {
		return classSinaWeibo;
	}

	public void setClassSinaWeibo(String classSinaWeibo) {
		this.classSinaWeibo = classSinaWeibo;
	}

	public String getRenrenUrl() {
		return renrenUrl;
	}

	public void setRenrenUrl(String renrenUrl) {
		this.renrenUrl = renrenUrl;
	}

	public String getClassRenren() {
		return classRenren;
	}

	public void setClassRenren(String classRenren) {
		this.classRenren = classRenren;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getClassTelphone() {
		return classTelphone;
	}

	public void setClassTelphone(String classTelphone) {
		this.classTelphone = classTelphone;
	}

	public String getOthContactBy() {
		return othContactBy;
	}

	public void setOthContactBy(String othContactBy) {
		this.othContactBy = othContactBy;
	}

	public String getClassOthContactBy() {
		return classOthContactBy;
	}

	public void setClassOthContactBy(String classOthContactBy) {
		this.classOthContactBy = classOthContactBy;
	}

	public String getCoverImageUrl() {
		return coverImageUrl;
	}

	public void setCoverImageUrl(String coverImageUrl) {
		this.coverImageUrl = coverImageUrl;
	}

	public String getOptFans() {
		return optFans;
	}

	public void setOptFans(String optFans) {
		this.optFans = optFans;
	}

	public String getOptFansDesc() {
		return optFansDesc;
	}

	public void setOptFansDesc(String optFansDesc) {
		this.optFansDesc = optFansDesc;
	}

	public String getOptFocuser() {
		return optFocuser;
	}

	public void setOptFocuser(String optFocuser) {
		this.optFocuser = optFocuser;
	}

	public String getOptFocuserDesc() {
		return optFocuserDesc;
	}

	public void setOptFocuserDesc(String optFocuserDesc) {
		this.optFocuserDesc = optFocuserDesc;
	}

	public String getOnlineStatusDesc() {
		return onlineStatusDesc;
	}

	public void setOnlineStatusDesc(String onlineStatusDesc) {
		this.onlineStatusDesc = onlineStatusDesc;
	}

	public String getIsHaveContact() {
		return isHaveContact;
	}

	public void setIsHaveContact(String isHaveContact) {
		this.isHaveContact = isHaveContact;
	}

	

}
