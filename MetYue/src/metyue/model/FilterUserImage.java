package metyue.model;

public class FilterUserImage implements java.io.Serializable{

	private static final long serialVersionUID = 4336684757491259666L;
	
	private Long pkey;
	private Long userPtr;
	private String imageSrcUrl;
	private String resizeImageSrcUrl;
	private String physicsPath;
	private String fileName;
	
	public FilterUserImage() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FilterUserImage(Long pkey, Long userPtr, String imageSrcUrl,
			String resizeImageSrcUrl, String physicsPath, String fileName) {
		super();
		this.pkey = pkey;
		this.userPtr = userPtr;
		this.imageSrcUrl = imageSrcUrl;
		this.resizeImageSrcUrl = resizeImageSrcUrl;
		this.physicsPath = physicsPath;
		this.fileName = fileName;
	}

	public Long getPkey() {
		return pkey;
	}

	public void setPkey(Long pkey) {
		this.pkey = pkey;
	}

	public Long getUserPtr() {
		return userPtr;
	}

	public void setUserPtr(Long userPtr) {
		this.userPtr = userPtr;
	}

	public String getImageSrcUrl() {
		return imageSrcUrl;
	}

	public void setImageSrcUrl(String imageSrcUrl) {
		this.imageSrcUrl = imageSrcUrl;
	}

	public String getResizeImageSrcUrl() {
		return resizeImageSrcUrl;
	}

	public void setResizeImageSrcUrl(String resizeImageSrcUrl) {
		this.resizeImageSrcUrl = resizeImageSrcUrl;
	}

	public String getPhysicsPath() {
		return physicsPath;
	}

	public void setPhysicsPath(String physicsPath) {
		this.physicsPath = physicsPath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
}
