﻿
var initMenuDs = [
	{
		text: "登录",
		encoded: true
	},
	{
		text: "我要注册",
		encoded: true
	},
	{
		text: "忘记密码",
		encoded: true
	},
	{
	    text: "魅约社区",
	    items: [
	            { text: "会员查找"}
	    ]
	},
	{
		text: "帮助文档",
		items: [
		        	{ text: "关于我们" }
		        ]
    }
];

var loginMenuDs = [
	{
	    text: "我的资料",
	    items: [
	            { text: "基本资料"},
	            { text: "交友宣言"},
	            { text: "联系方式" },
	            { text: "我的图片" }
	    ]
	},
	{
	    text: "魅约社区",
	    items: [
	            { text: "会员查找"},
	            { text: "我的关注"},
	            { text: "我的粉丝" }
	    ]
	},
	{
	    text: "消息",
	    items: [
	            { text: "消息面板"}
	    ]
	},
	{
	    text: "帮助文档",
	    items: [
	            	{ text: "关于我们" }
	            ]
	},
	{
	    text: "退出",
	    encoded: true
	}
];

var sexData = [
   { text: "男生", value: "1" },
   { text: "女生", value: "2" }
];

var jobData = [
               { text: "办公行政", value: "1" },
               { text: "销售业务", value: "2" },
               { text: "产线工人", value: "3" },
               { text: "技术人员", value: "4" },
               { text: "在校学生", value: "5" },
               { text: "其它", value: "6" }
];

var marryData = [
       { text: "未婚", value: "1" },
   { text: "已婚", value: "2" },
   { text: "丧偶", value: "3" }
];

var educationData = [
   { text: "初中", value: "1" },
   { text: "高中", value: "2" },
   { text: "中专", value: "3" },
   { text: "专科", value: "4" },
   { text: "本科", value: "5" },
   { text: "硕士", value: "6" },
   { text: "博士", value: "7" },
   { text: "其它", value: "8" }
];


var purposeData = [
   { text: "征友，寻找拥有共同兴趣的朋友", value: "1" },
   { text: "征婚，寻觅人生的另一半", value: "2" },
   { text: "合作，寻求生活或事业的新激情", value: "3" },
   { text: "帮助，寻求经济或道义上的支持", value: "4" }
];

var experData = [
   { text: "没有发生过，想尝试一下", value: "1" },
   { text: "曾经发生过，乐意再尝试", value: "2" },
   { text: "已尝试多次，喜欢这感觉", value: "3" },
   { text: "特别迷恋它，很想经常玩", value: "4" }
];

var mannerData = [
  { text: "先见一面相互认识", value: "1" },
  { text: "一切随缘而为吧", value: "2" },
  { text: "过了今晚，我们没有明天", value: "3" },
  { text: "彼此满意可做长期打算", value: "4" },
  { text: "保密，不想回答", value: "5" }
];

var sexQueryData = [
	{ text: "不限", value: "0" },
	{ text: "男生", value: "1" },
	{ text: "女生", value: "2" }
];

var purposeQueryData = [
    { text: "不限", value: "0" },
    { text: "征友，寻找拥有共同兴趣的朋友", value: "1" },
    { text: "征婚，寻觅人生的另一半", value: "2" },
    { text: "合作，寻求生活或事业的新激情", value: "3" },
    { text: "帮助，寻求经济或道义上的支持", value: "4" }
];
