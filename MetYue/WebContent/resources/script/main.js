
function extendUrl01(){	
	
	return "&AppSource=" + appSource + "&etc=" + new Date().getTime();
}

function extendUrl02(){	
	
	return "?AppSource=" + appSource + "&etc=" + new Date().getTime();
}

function initFixPageHeight() { 
	
    var nRealPageHeight = $(window).height() - $("#headWideDiv").height();     
    if (parseInt(nRealPageHeight) > parseInt("420")) {    	
        $("#contentWideDiv").height(nRealPageHeight - 2);  
        
    } else {
    	
        $("#contentWideDiv").height(420);
        
    }	
}

function t3_td2_view(imageUrl){	
	
	openNewSrcWindow(imageUrl);
}

function t3_td2_del(imagePtr){	
	
	$('#photoWindowLoading').show();
    $.ajax({ 
	  	url: "user/deleteImageFile.json" + extendUrl02() + "&PhotoListPtr=" + imagePtr, 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){
  				
  				if(json.result=='SUCCESS'){  					

  					//重新获取图片记录，且显示至指定页数
                    photoListViewCurrentPage = photoListViewDS.page();                    
                    bindPhotoListView(); 
                    
  				}else{  					
                    $("#photoListViewValidator").text(json.context)
	                    .removeClass("valid")
	                	.addClass("invalid"); 
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {
            $("#photoListViewValidator").text("删除失败")
	            .removeClass("valid")
	        	.addClass("invalid"); 
        },
	  	complete: function(x) {
	  		$('#photoWindowLoading').hide();
	  	} 
	});	
	
}

function t3_td2_public(imagePtr){
	
	$('#photoWindowLoading').show();
    $.ajax({ 
	  	url: "user/displayImage.json" + extendUrl02() + "&PhotoListPtr=" + imagePtr, 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){
  				
  				if(json.result=='SUCCESS'){  					

  					//重新获取图片记录，且显示至指定页数
                    photoListViewCurrentPage = photoListViewDS.page();                    
                    bindPhotoListView(); 
                    
  				}else{  					
                    $("#photoListViewValidator").text(json.context)
	                    .removeClass("valid")
	                	.addClass("invalid"); 
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {
            $("#photoListViewValidator").text("操作失败")
	            .removeClass("valid")
	        	.addClass("invalid"); 
        },
	  	complete: function(x) {
	  		$('#photoWindowLoading').hide();
	  	} 
	});	
}

function t3_td2_cover(imagePtr){	
	
	$('#photoWindowLoading').show();
    $.ajax({ 
	  	url: "user/changePhotoCover.json" + extendUrl02() + "&PhotoListPtr=" + imagePtr, 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){
  				
  				if(json.result=='SUCCESS'){  					

  					//重新获取图片记录，且显示至指定页数
                    photoListViewCurrentPage = photoListViewDS.page();                    
                    bindPhotoListView(); 
                    
  				}else{  					
                    $("#photoListViewValidator").text(json.context)
	                    .removeClass("valid")
	                	.addClass("invalid"); 
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {
            $("#photoListViewValidator").text("操作失败")
	            .removeClass("valid")
	        	.addClass("invalid"); 
        },
	  	complete: function(x) {
	  		$('#photoWindowLoading').hide();
	  	} 
	});	
}

function doneMenu_init(){
	
	optMenu = $("#menuUL").kendoMenu({ 
		
        dataSource: initMenuDs,        
        select: onMenuSelect
	});
}

function doneMenu_login(){
	
	optMenu = $("#menuUL").kendoMenu({
		
        dataSource: loginMenuDs,         
        select: onMenuSelect
	});
}

function onMenuSelect(e){
	
	var menuTitle = $(e.item).children(".k-link").text();
	
	if(menuTitle=="登录"){
		
		showLoginWindows();
		
	}else if(menuTitle=="我要注册"){
		
		showRegisterWindows();
		
	}else if(menuTitle=="忘记密码"){
		
		showForgetWindows();
		
	}else if(menuTitle=="基本资料"){
		
		showBaseUserInfoWindows();
		
	}else if(menuTitle=="交友宣言"){
		
		showBeFriendWindows();
		
	}else if(menuTitle=="联系方式"){
		
		showContactWindows();
		
	}else if(menuTitle=="我的图片"){
		
		showPhotoWindows();
		
	}else if(menuTitle=="退出"){
		
		logout();	
	}else if(menuTitle=="会员查找"){
		
		showMemberQueryWindows();	
	}else if(menuTitle=="我的关注"){
		
		memberQueryUrl = "member/getMemberCenterList.json" + extendUrl02()
				+ "&USER_PTR="
				+ "&KEY_WORDS="
				+ "&QUERY_SEX="
				+ "&QUERY_PLAN="
				+ "&QUERY_AGE1="
				+ "&QUERY_AGE2="
				+ "&OPT_FANS=F"
				+ "&OPT_FOCUSER=T"
				+ "&OPT_ONLINE="
				+ "&OPT_HAVE_CONTACT=";
		
		showMemberCenterWindowsQuery(memberQueryUrl);	
		
	}else if(menuTitle=="我的粉丝"){
		
		memberQueryUrl = "member/getMemberCenterList.json" + extendUrl02()
				+ "&USER_PTR="
				+ "&KEY_WORDS="
				+ "&QUERY_SEX="
				+ "&QUERY_PLAN="
				+ "&QUERY_AGE1="
				+ "&QUERY_AGE2="
				+ "&OPT_FANS=T"
				+ "&OPT_FOCUSER=F"
				+ "&OPT_ONLINE="
				+ "&OPT_HAVE_CONTACT=";

		showMemberCenterWindowsQuery(memberQueryUrl);	
		
	}else if(menuTitle=="关于我们"){
		
		showAboutMeWindows();
		
	}else if(menuTitle=="消息面板"){
		
		showTalkPannelWindows();
	}
}

//初始化页面：自动判断用户是否已经在线
function initPage(){
	
	$('#loading').show();
    $.ajax({ 
	  	url: "user/checkOnline.json" + extendUrl02(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){
  				
  				if(json.result=='ON_LINE'){  					
  					closeLoginWindows();
  					doneMenu_login();
  					run_talkTipsTimer();
  		            	  					
  				}else if(json.result=='OFF_LINE'){  					
  					doneMenu_init();
  					showLoginWindows();
  					
  				}else{  					
  					doneMenu_init();
  					showLoginWindows();
  					
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {
	  		
	  			doneMenu_init();	  			
	  			showLoginWindows();
        },
	  	complete: function(x) {
	  		$('#loading').hide();
	  	} 
	});
}

//用户退出
function logout(){
	
	$('#loading').show();
    $.ajax({ 
	  	url: "user/logout.json" + extendUrl02(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){  				
  				if( (json.result=='AUTO_OFF_LINE') || (json.result=='SUCCESS') ){  					
  					doneMenu_init();
  					closeAllWindows();
  					showLoginWindows();
  					
  					stop_talkTipsTimer();
  					
  				} else{  					
  					showMsgBoxWindows(json.context);
  					
  				}				
  			}
	  	}, 
	  	error: function(x, e) {	  		
	  		showMsgBoxWindows(errorMsg);	  		
        },
	  	complete: function(x) {
	  		$('#loading').hide();	  		
	  	} 
	});
	
}

//提示窗口
function createMsgboxWindows(){   
	
    if(optMsgBoxWindows == null){        
    	optMsgBoxWindows = $("#msgBoxWindowsDiv").kendoWindow({                
            width: "320px",
            height: "120px",
            title: "提示",
            resizable: false
        });      
    	
        $("#btnConfrim").kendoButton({        	
            click: function onClick(e) {  
            	
            	closeMsgBoxWindows();
            }
        });
    }
}

function clearMsgBoxFrom(){	
	
    $("#msgBoxValidator").text("");
}

function addMsgBoxFrom(tips){	
	
    $("#msgBoxValidator").text(tips);
}

function showMsgBoxWindows(tips){ 
	
    if(optMsgBoxWindows == null){        
    	createMsgboxWindows();
    	clearMsgBoxFrom();
    }   
    
    addMsgBoxFrom(tips);    
    optMsgBoxWindows.data("kendoWindow").open();
}

function closeMsgBoxWindows(){ 	
	
    if(optMsgBoxWindows != null){      
    	
    	optMsgBoxWindows.data("kendoWindow").close();
    }    
}



//用户登录
function makeLoginUrl(){	
    var email = $("#txtLoginEmail").val();
    var pwd = $("#txtLoginPwd").val();
    
	return "?Email=" + email + "&Pwd=" + pwd + extendUrl01();
}

function loginAjax(){
    
	$('#loginLoading').show();
    $.ajax({ 
	  	url: "user/login.json" + makeLoginUrl(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){
	  		
  			if((json != null)&&(json!= "undefined")){  				
  				if(json.result=='SUCCESS'){
  					doneMenu_login();
  					//closeLoginWindows();
  					closeAllWindows();  
  					
  					//自动获取消息条数
  					run_talkTipsTimer();
  					
  				}else{  					
                    $("#loginValidator").text(json.context)
                        .removeClass("valid")
                    	.addClass("invalid");                    
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {	  		
				doneMenu_init();
        },
	  	complete: function(x) {	  		
	  		$('#loginLoading').hide();	  		
	  	} 
	});
}

function createLoginWindows(){
    
    if(optLoginWindows == null){
        
    	optLoginWindows = $("#loginWindowsDiv").kendoWindow({                
            width: "600px",
            height: "380px",
            title: "用户登录",
            resizable: false
        });
        
        $("#btnLogin").kendoButton({        	
            click: function onClick(e) {                 
                var loginName = $("#txtLoginEmail").val();
                var loginPwd = $("#txtLoginPwd").val();
                
                if((loginName.length == 0) || (loginPwd.length == 0)){                      
                    $("#loginValidator").text("请输入帐号和密码")
                    	.removeClass("valid")
                    	.addClass("invalid");                    
                }else{
                	loginAjax();                    
                }
                
            }
        });
    }
}

function clearLoginFrom(){    
	
    $("#txtLoginEmail").val("");    
    $("#txtLoginPwd").val("");    
    $("#loginValidator").text("")    
        .removeClass("invalid")
        .removeClass("valid");
}

function showLoginWindows(){  
	
    if(optLoginWindows == null){        
        createLoginWindows();
    }    
    clearLoginFrom();    
    optLoginWindows.data("kendoWindow").open();
}

function closeLoginWindows(){ 
	
    if(optLoginWindows != null){        
    	optLoginWindows.data("kendoWindow").close();
    }    
}



//快速注册
function createRegisterWindows(){
    
    if(optQuickRegisterWindows == null){
    	
        //
        optQuickRegisterWindows = $("#quickRegisterWindowsDiv").kendoWindow({
            
            width: "600px",
            height: "380px",
            title: "注册帐号",
            resizable: false,
            actions: ["Close"]            
        });
        
        //
    	validatorRegisterWindow = $("#quickRegisterFormDiv").kendoValidator({
            rules: {
            	Charet: function (input) {
                    if (input.is("[data-charet-msg-require]") && input.val() == "") {
                    	return false;                        
                    }else if (input.is("[data-charet-msg-20]")) {
                    	var value = input.val();
                    	if ((value.length > 20) || (value=="") ) {
                    		return false;
                    	}
                    }else if (input.is("[data-charet-msg-50]")) {
                    	var value = input.val();
                    	if ((value.length > 50) || (value=="") ) {
                    		return false;
                    	}
                    }else if (input.is("[data-charet-msg-email]")){
                    	var value = input.val();                    	
                    	var reg = /^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/;
                    	if(!reg.test(value)){  
                			return false; 
                        }
                    }                  
                    
                    return true;                    
                }
            }
        }).data("kendoValidator");    	
    	
    	//
        $("#txtRegSex").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: sexData,
            index: 1
        });
        
        //
        $("#btnQuickRegister").kendoButton({
            click: function onClick(e) {

                if ((validatorRegisterWindow.validate()==false) || (requireRegWindows() == false)) { 
                	
                    $("#registerValidator").text("内容格式有误，请仔细填写")
                        .removeClass("valid")
                        .addClass("invalid");
                    
                    return false;
                }else{

                	regAjax();
                }
                
                return true;
                
            }
        });
        
        //
        
    } 
    
}

function makeRegUrl(){
	
    var emi = $("#txtRegEmail").val();
    var nam = $("#txtRegName").val();
    var pwd = $("#txtRegPwd").val();
    var sex = $("#txtRegSex").data("kendoDropDownList").value();
    var que = $("#txtRegQuestion").val();
    var ans = $("#txtRegAnswer").val();

    return "?Name=" + encode2(nam) + "&Email=" + emi + "&Pwd=" + encode2(pwd) + "&Sex=" + sex + "&Question=" + encode2(que) + "&Answer=" + encode2(ans) + extendUrl01();
 
}

function regAjax(){
	
	$('#registerLoading').show();
    $.ajax({ 
	  	url: "user/register.json" + makeRegUrl(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){
	  		
  			if((json != null)&&(json!= "undefined")){  				
  				if(json.result=='SUCCESS'){
  					doneMenu_login();
  					closeRegisterWindows();
  					closeLoginWindows(); 					
  				}else{  					
                    $("#registerValidator").text(json.context)
                        .removeClass("valid")
                    	.addClass("invalid");                    
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {	  		
				doneMenu_init();
        },
	  	complete: function(x) {	  		
	  		$('#registerLoading').hide();	  		
	  	} 
	});
}

function requireRegWindows(){
    
    var emi = $("#txtRegEmail").val();
    var nam = $("#txtRegName").val();
    var pwd = $("#txtRegPwd").val();
    var sex = $("#txtRegSex").data("kendoDropDownList").value();
    var que = $("#txtRegQuestion").val();
    var ans = $("#txtRegAnswer").val();
    
    if((emi.length == 0) || (nam.length == 0)|| (pwd.length == 0)|| (que.length == 0)|| (ans.length == 0) || (sex.length == 0) ){
        
       return false; 
    }
    
    return true;
    
}

function clearRegWindows(){
    
    $("#txtRegEmail").val("");
    $("#txtRegName").val("");
    $("#txtRegPwd").val("");
    $("#txtRegSex").data("kendoDropDownList").value("1");
    $("#txtRegQuestion").val("");
    $("#txtRegAnswer").val("");
    
}

function showRegisterWindows(){
    
    if(optQuickRegisterWindows == null){
        
        createRegisterWindows();
    }
    
    clearRegWindows();
    
    optQuickRegisterWindows.data("kendoWindow").open();
}

function closeRegisterWindows(){
    
    if(optQuickRegisterWindows != null){
        
    	optQuickRegisterWindows.data("kendoWindow").close();
    }    
    
}


//忘记密码
function makeForgetPwdUrl(){	
    
    var email = $("#txtForgetEmail").val();
    var answer = $("#txtForgetAnswer").val(); 
    
	return "?Email=" + email + "&Answer=" + answer + extendUrl01();
}

function forgetPwdAjax(){
    
	$('#forgetLoading').show();
    $.ajax({ 
	  	url: "user/checkUserAnswer.json" + makeForgetPwdUrl(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){
	  		
  			if((json != null)&&(json!= "undefined")){  				
  				if(json.result=='SUCCESS'){
                    $("#forgetValidator").text(json.context)
                    .removeClass("invalid")
                	.addClass("valid"); 
  		            	  					
  				}else{  					
                    $("#forgetValidator").text(json.context)
                        .removeClass("valid")
                    	.addClass("invalid");                    
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {	  		
				doneMenu_init();
        },
	  	complete: function(x) {	  		
	  		$('#forgetLoading').hide();	  		
	  	} 
	});
}

function makeQuesUrl(){
	
	var email = $("#txtForgetEmail").val();	
	return "?Email=" + email + extendUrl01();
}

function onForgetEamilBlur(){
	
	var email = $("#txtForgetEmail").val();	
	if(email.length == 0){
		
		return false;
	}
	
    $("#forgetValidator").text("")    
		    .removeClass("invalid")
		    .removeClass("valid");
	
	$('#forgetLoading').show();
    $.ajax({ 
	  	url: "user/getUserQuestion.json" + makeQuesUrl(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){
	  		
  			if((json != null)&&(json!= "undefined")){  				
  				if(json.result=='SUCCESS'){  		
  					
  					$("#labForgetQuestion").text(json.context);
  					
  				}else{  	
  					
                    $("#forgetValidator").text(json.context)
                        .removeClass("valid")
                    	.addClass("invalid");                    
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {},
	  	complete: function(x) {	  		
	  		$('#forgetLoading').hide();	  		
	  	} 
	});
    
}


function createForgetWindows(){
    
    if(optForgetWindows == null){
        
    	optForgetWindows = $("#forgetWindowsDiv").kendoWindow({                
            width: "600px",
            height: "280px",
            title: "忘记密码",
            resizable: false,
            actions: ["Close"] 
        });        

        //
    	validatorForgetWindow = $("#forgetFormDiv").kendoValidator({
            rules: {
                charet: function (input) {                	
                    var txt = input.val();
                    
                    if (input.is("[data-charet-msg-50]")) {
                    	if (( txt == "") || (txt.length >=50)){
                    		return false;                    		
                    	}                    	                        
                    }else if (input.is("[data-charet-msg-email]")){
                    	if( (txt=="") || (txt.indexOf("@") == -1)){
                    		return false;
                    	}
                    }
                    
                    return true;
                }
            }
        
        }).data("kendoValidator"); 
        
    	$("#txtForgetEmail").blur(function() {
    		onForgetEamilBlur();
    	});
    	
        $("#btnForgetPwd").kendoButton({        	
            click: function onClick(e) {
            	
            	if(validatorForgetWindow.validate()==false){        		
                    $("#forgetValidator").text("请填写邮箱地址和密码问题的答案")
                	.removeClass("valid")
                	.addClass("invalid"); 
            	}else{
                	forgetPwdAjax();                    
                }         
            	           	
            	return true;
            }
        });
    }
}

function clearForgetFrom(){    
	
	$("#txtForgetEmail").val(""); 
	$("#labForgetQuestion").text(""); 
	$("#txtForgetAnswer").val(""); 
  
    $("#forgetValidator").text("")    
        .removeClass("invalid")
        .removeClass("valid");
}

function showForgetWindows(){  
	
    if(optForgetWindows == null){        
    	createForgetWindows();
    }    
    
    clearForgetFrom();    
    optForgetWindows.data("kendoWindow").open();
}

function closeForgetWindows(){ 
	
    if(optForgetWindows != null){        
    	optForgetWindows.data("kendoWindow").close();
    }    
}



//基本资料窗口
function makeBaseInfoUrl(){
	
	var address = $("#txtBaseInfoAddress").val();	
    var age = $("#txtBaseInfoAge").data("kendoNumericTextBox").value();
    var height = $("#txtBaseInfoHeight").data("kendoNumericTextBox").value();
    var weight = $("#txtBaseInfoWeight").data("kendoNumericTextBox").value();
    var marry = $("#txtBaseInfoMarry").data("kendoDropDownList").value();
    var job = $("#txtBaseInfoJob").data("kendoDropDownList").value();
    var education = $("#txtBaseInfoEducation").data("kendoDropDownList").value();
    
    return "?Age=" + age
			+ "&Address=" + encode2(address)
			+ "&Height=" + height
			+ "&Weight=" + weight
			+ "&Marry=" + marry
			+ "&Job=" + job
			+ "&Education=" + education  
			+ extendUrl01();    
}


function saveBaseInfoAjax(){
	
	$('#baseUserInfoLoading').show();
	
    $.ajax({ 
	  	url: "user/saveUserBaseInfo.json" + makeBaseInfoUrl(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){  				
                $("#baseUserInfoValidator").text(json.context)
                .removeClass("invalid")
            	.addClass("valid");
  			}
	  	}, 
	  	error: function(x, e) {},
	  	complete: function(x) {	  		
	  		$('#baseUserInfoLoading').hide();	  		
	  	} 
	});
	
}

function bindBaseInfo(obj){
	
	clearBaseUserInfoWindows();
	
	$("#txtBaseInfoAddress").val(obj.currentAddress);
	
    $("#txtBaseInfoAge").data("kendoNumericTextBox").value(obj.age);
    $("#txtBaseInfoHeight").data("kendoNumericTextBox").value(obj.height);
    $("#txtBaseInfoWeight").data("kendoNumericTextBox").value(obj.weight);
    
    $("#txtBaseInfoMarry").data("kendoDropDownList").value(obj.marry);
    $("#txtBaseInfoJob").data("kendoDropDownList").value(obj.job);
    $("#txtBaseInfoEducation").data("kendoDropDownList").value(obj.education);
}

function getBaseInfoAjax(){
	
	$('#baseUserInfoLoading').show();
	
    $.ajax({ 
	  	url: "user/getUserBaseInfo.json" + extendUrl02(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){
  				bindBaseInfo(json);	
  			}else{
                $("#baseUserInfoValidator").text(json.context)
                    .removeClass("valid")
                	.addClass("invalid");                    
	  		}
	  	}, 
	  	error: function(x, e) {},
	  	complete: function(x) {	  		
	  		$('#baseUserInfoLoading').hide();	  		
	  	} 
	});
	
}

function clearBaseUserInfoWindows(){
    
	$("#txtBaseInfoAddress").val("");
	
    $("#txtBaseInfoAge").data("kendoNumericTextBox").value("20");
    $("#txtBaseInfoHeight").data("kendoNumericTextBox").value("160");
    $("#txtBaseInfoWeight").data("kendoNumericTextBox").value("50");
    
    $("#txtBaseInfoMarry").data("kendoDropDownList").value("0");
    $("#txtBaseInfoJob").data("kendoDropDownList").value("0");
    $("#txtBaseInfoEducation").data("kendoDropDownList").value("5");
    
    $("#baseUserInfoValidator").text("")
		    .removeClass("valid")
			.addClass("invalid");  
}

function checkBaseUserForm(){
	
	var result = true;
	
    var age = $("#txtBaseInfoAge").data("kendoNumericTextBox").value();
    var height = $("#txtBaseInfoHeight").data("kendoNumericTextBox").value();
    var weight = $("#txtBaseInfoWeight").data("kendoNumericTextBox").value();
    
    if((age==null) || (height==null) || (weight==null) ){

    	result = false;
    	
    }else if((age < 18) || (height < 120) || (weight < 20)){
    	
    	result = false;
    }
    
    return result;
}

function createBaseUserInfoWindows(){
    
    if(optBaseUserInfoWindows == null){
        
        //
    	optBaseUserInfoWindows = $("#baseUserInfoWindowsDiv").kendoWindow({
            
            width: "600px",
            height: "420px",
            title: "基本资料",
            resizable: false,
            actions: ["Close"]            
        });
        
    	$("#txtBaseInfoAge").kendoNumericTextBox({format: "#",decimals: 0,upArrowText: "More",downArrowText: "Less"});
        $("#txtBaseInfoHeight").kendoNumericTextBox({format: "#",decimals: 0});
        $("#txtBaseInfoWeight").kendoNumericTextBox({format: "#",decimals: 0});        
        
        $("#txtBaseInfoMarry").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: marryData,
            index: 0
        });
        
        $("#txtBaseInfoJob").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: jobData,
            index: 0
        });
        
        $("#txtBaseInfoEducation").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: educationData,
            index: 5
        });
        
        //
        validatorBaseUserInfoWindow = $("#baseUserInfoFormDiv").kendoValidator({
            rules: {
            	charet: function (input) {              	
                    var txt = input.val();                    
                    if (input.is("[data-charet-50-msg]")) {  
                    	if (( txt == "") || (txt.length >=50)) { 
                    		return false; 
                    	}
                    }
                    
                    return true;
                }
            }
        }).data("kendoValidator"); 
    
        //
        $("#btnSaveBaseUserInfo").kendoButton({
            click: function onClick(e) {                 
            	            	
            	if(checkBaseUserForm()==false){
            		
                    $("#baseUserInfoValidator").text("虽然不用自报三围，至少年龄、身高、体重是要填填的嘛，^_^")
	                    .removeClass("valid")
	                    .addClass("invalid");
                    
                    return false;
            	}
            	
            	if (validatorBaseUserInfoWindow.validate()==false) {
            		
                    $("#baseUserInfoValidator").text("内容格式有误，请仔细填写")
		                    .removeClass("valid")
		                    .addClass("invalid");   
                    
                    return false;
                    
            	}else{
            		
            		saveBaseInfoAjax();
            	}
            	
            	return true;
            }
        });
        
    } 
    
}

function showBaseUserInfoWindows(){ 
	
    if(optBaseUserInfoWindows == null){        
        createBaseUserInfoWindows();
    }    
    
    clearBaseUserInfoWindows(); 
    
    optBaseUserInfoWindows.data("kendoWindow").open();
        
    getBaseInfoAjax();
}

function closeBaseUserInfoWindows(){
	
    if(optBaseUserInfoWindows != null){   
    	
    	optBaseUserInfoWindows.data("kendoWindow").close();
    }    
}


//交友宣言
function makePurposeInfoUrl(){
    
	var plan = $("#txtBeFriendPurpose").data("kendoDropDownList").value();
    var express = $("#txtBeFriendExper").data("kendoDropDownList").value();
    var manner = $("#txtBeFriendManner").data("kendoDropDownList").value();
    var desc = $("#txtBeFriendExplain").val();
    
    return "?Plan=" + plan
			+ "&Express=" + express
			+ "&Manner=" + manner
			+ "&Descript=" + encode2(desc)
			+ extendUrl01();    
}


function savePurposeInfoAjax(){
	
	$('#beFriendLoading').show();
	
    $.ajax({ 
	  	url: "user/saveUserPurposeInfo.json" + makePurposeInfoUrl(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){  				
                $("#beFriendValidator").text(json.context)
		                .removeClass("invalid")
		            	.addClass("valid");
  			}
	  	}, 
	  	error: function(x, e) {},
	  	complete: function(x) {	  		
	  		$('#beFriendLoading').hide();	  		
	  	} 
	});
	
}

function bindPurposeInfo(obj){

    $("#txtBeFriendPurpose").data("kendoDropDownList").value(obj.plan);
    $("#txtBeFriendExper").data("kendoDropDownList").value(obj.express);
    $("#txtBeFriendManner").data("kendoDropDownList").value(obj.manner);
    
    $("#txtBeFriendExplain").val(obj.purposeDesc);
    
}

function getPurposeInfoAjax(){
	
	$('#beFriendLoading').show();
	
    $.ajax({ 
	  	url: "user/getUserPurposeInfo.json" + extendUrl02(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){
  				bindPurposeInfo(json);	
  			}else{
                $("#beFriendValidator").text(json.context)
                    .removeClass("valid")
                	.addClass("invalid");                    
	  		}
	  	}, 
	  	error: function(x, e) {},
	  	complete: function(x) {	  		
	  		$('#beFriendLoading').hide();	  		
	  	} 
	});
	
}

function createBeFriendWindows(){
    
    if(optBeFriendWindows == null){
        
    	optBeFriendWindows = $("#beFriendWindowsDiv").kendoWindow({
            
            width: "600px",
            height: "400px",
            title: "交友宣言",
            resizable: false,
            actions: ["Close"]            
        });
        
		$("#txtBeFriendPurpose").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: purposeData,
            index: 0
        });
        
        $("#txtBeFriendExper").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: experData,
            index: 0
        });
        
        $("#txtBeFriendManner").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: mannerData,
            index: 0
        });
        
        // 
    	validatorBeFriendWindow = $("#beFriendWindowsFormDiv").kendoValidator({
    		rules: {
            	charet: function (input) {              	
                    return true;
                }
    		}
    		
    	}).data("kendoValidator");        
    
        //
        $("#btnSaveBeFriend").kendoButton({
            click: function onClick(e) {                 
                
                var validator = validatorBeFriendFrom();
                
                if(validator){
                    
                	savePurposeInfoAjax();
                }
            }
        });
        
    } 
    
}

function validatorBeFriendFrom(){
    
    var result = true;
    
    var txtExplain = $("#txtBeFriendExplain").val();
    
    if( txtExplain.length > 200 ){
        
       $("#beFriendWindowStatus").text("交友宣言字数过长，请限制在 200 字内")
            .removeClass("valid")
            .addClass("invalid"); 
        
        result = false;
    }
    
    return result;
}

function clearBeFriendWindows(){
    
    $("#txtBeFriendPurpose").data("kendoDropDownList").value("0");
    $("#txtBeFriendExper").data("kendoDropDownList").value("0");
    $("#txtBeFriendManner").data("kendoDropDownList").value("0");
    
    $("#txtBeFriendExplain").val("");
}

function showBeFriendWindows(){
    
    if(optBeFriendWindows == null){
        
        createBeFriendWindows();
    }
    
    clearBeFriendWindows();
    
    optBeFriendWindows.data("kendoWindow").open();
    
    getPurposeInfoAjax();
}

function closeBeFriendWindows(){
    
    if(optBeFriendWindows != null){
        
    	optBeFriendWindows.data("kendoWindow").close();
    }
}



//联系资料窗口
function makeContactInfoUrl(){
	
	var qq = $("#txtContactQQ").val();
	var weiXien = $("#txtContactWeiXien").val();
	var weiBo = $("#txtContactWeiBo").val();
	var renRen = $("#txtContactRenRen").val();
	var tel = $("#txtContactTel").val();
	var other = $("#txtContactOther").val();
	
	return "?QQ=" + qq
			+ "&WeiXien=" + weiXien
			+ "&WeiBo=" + weiBo
			+ "&RenRen=" + renRen
			+ "&Tel=" + tel
			+ "&Other=" + other
			+ extendUrl01();
	
}

function saveContactInfoAjax(){
	
	$('#contactLoading').show();
	
    $.ajax({ 
	  	url: "user/saveUserContactInfo.json" + makeContactInfoUrl(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: false,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){  				
                $("#contactValidator").text(json.context)
		                .removeClass("invalid")
		            	.addClass("valid");
  			}
	  	}, 
	  	error: function(x, e) {},
	  	complete: function(x) {	  		
	  		$('#contactLoading').hide();	  		
	  	} 
	});
}

function bindContactInfo(obj){
    
    $("#txtContactQQ").val(obj.tencentNum);
    $("#txtContactWeiXien").val(obj.tencentWeixien);
    $("#txtContactWeiBo").val(obj.sinaWeibo);
    $("#txtContactRenRen").val(obj.renrenUrl);
    $("#txtContactTel").val(obj.tel);
    $("#txtContactOther").val(obj.othContactBy);
}

function getContactInfoAjax(){
	
	$('#contactLoading').show();
	
    $.ajax({ 
	  	url: "user/getUserContactInfo.json" + extendUrl02(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){
  				bindContactInfo(json);	
  			}else{
                $("#beFriendValidator").text(json.context)
                    .removeClass("valid")
                	.addClass("invalid");                    
	  		}
	  	}, 
	  	error: function(x, e) {},
	  	complete: function(x) {	  		
	  		$('#contactLoading').hide();	  		
	  	} 
	});
	
}

function createContactWindows(){
    
    if(optContactWindows == null){
        
    	optContactWindows = $("#contactWindowsDiv").kendoWindow({            
            width: "600px",
            height: "400px",
            title: "联系资料",
            resizable: false,
            actions: ["Close"]            
        });        
        
        // 
    	validatorContactWindow = $("#contactWindowsFormDiv").kendoValidator({
            rules: {
                isCharet20: function (input) {
                    var txt = input.val();
                    if ( input.is("[data-charet-msg-20]") && (txt.length >=20) )  {  
                    	return false;
                    }else if ( input.is("[data-charet-msg-100]") && (txt.length >=100) )  {  
                    	return false;
                    }else if ( input.is("[data-charet-msg-url]") )  {                    	
                    	//var reg = /http://([\w-]+\.)+[\w-]+(/[\w-./?%&=]*)?/;/////^[A-Za-z0-9]+.[A-Za-z0-9]+[/=?%-&_~`@[]':+!]*([^<>"])*$/;
                    	//if(!reg.test(txt)){  
                		//	return false; 
                        //}
                    }
                    
                    return true;
                }
            }
        
        }).data("kendoValidator");        
    
        //
        $("#btnSaveContact").kendoButton({
            click: function onClick(e) {            	
            	if (validatorContactWindow.validate()==false) {
            		
                    $("#contactValidator").text("内容格式有误，请仔细填写")
		                    .removeClass("valid")
		                    .addClass("invalid");                    
                    return false;
                    
            	}else{
            		
            		saveContactInfoAjax();
            	}
            }
        });
        
    } 
    
}


function clearContactWindows(){
    
    $("#txtContactQQ").val("");
    $("#txtContactWeiXien").val("");
    $("#txtContactWeiBo").val("");
    $("#txtContactRenRen").val("");
    $("#txtContactTel").val("");
    $("#txtContactOther").val("");

}

function showContactWindows(){
    
    if(optContactWindows == null){
        
        createContactWindows();
    }
    
    clearContactWindows();
    
    optContactWindows.data("kendoWindow").open();
    
    getContactInfoAjax();
}

function closeContactWindows(){
    
    if(optContactWindows != null){
        
    	optContactWindows.data("kendoWindow").close();
    }
}


//图片窗口
function createPhotoWindows(){
    
    if(optPhotoWindows == null){ 
    	
    	optPhotoWindows = $("#photoWindowsDiv").kendoWindow({            
            width: "800px",
            height: "530px",
            title: "图片浏览",
            resizable: false,
            actions: ["Upload", "Refresh", "Close"],
            refresh: function(e){
                photoListViewCurrentPage = 1;
                bindPhotoListView();
            }
        });  
    	
        //Upload images button
        $("#photoWindowsDiv").data("kendoWindow").wrapper.find(".k-i-upload").click(function(e){
        	showUploadImageWindows();
            e.preventDefault();
        }); 
    } 
    
}

function showPhotoWindows(){
    
    if(optPhotoWindows == null){
        
        createPhotoWindows();
    }
    
    optPhotoWindows.data("kendoWindow").open();
    
    photoListViewCurrentPage = 1;
    
    bindPhotoListView();
}

function closePhotoWindows(){
    
    if(optPhotoWindows != null){
        
    	optPhotoWindows.data("kendoWindow").close();
    }
   
}

function bindPhotoListView(){
    
	photoListViewDS = new kendo.data.DataSource({
		page: photoListViewCurrentPage,
    	pageSize: 8,
        transport: {
            read: {
                url: "user/getUserPhotoListInfo.json" + extendUrl02(), 
                dataType: "json"
            }
        }
    });
    
	//photoListViewDS.read();
	
	$("#photoListViewPager").kendoPager({
        dataSource: photoListViewDS
    });
    
    $("#photoListView").kendoListView({
        dataSource: photoListViewDS,
        template: kendo.template($("#centerPhotoTemplate").html())
    });
     
}

//图片上传完毕，刷新图片窗口
function refreshPhotoWindows(){
	
    if(optPhotoWindows != null){
        
    	photoListViewCurrentPage = photoListViewDS.page();
    	
    	bindPhotoListView();
    }
}


//Upload inmages window
function createUploadImageWindows(){
	
    if(optUploadWidows == null){
    	
    	optUploadWidows = $("#uploadImageWindowsDiv").kendoWindow({            
            width: "420px",
            height: "320px",
            title: "图片上传",
            resizable: false,
            actions: ["Close"]            
        });
    }
}

function showUploadImageWindows(){
    
    if(optUploadWidows == null){  
    	
    	createUploadImageWindows();
    	
    }
    
    optUploadWidows.data("kendoWindow").open();    
}

function closeUploadImageWindows(){
    
    if(optUploadWidows != null){   
    	
    	optUploadWidows.data("kendoWindow").close();
    	
    }
   
}

//会员查询窗口
function createMemberQueryWindows(){
    
    if(optMemberQueryWindows == null){        
    	optMemberQueryWindows = $("#memberCenterQueryWindowsDiv").kendoWindow({            
            width: "580px",
            height: "320px",
            title: "会员查询",
            resizable: true,
            actions: ["Close"]            
        }); 
        
        $("#txtMemberQueryAge1").kendoNumericTextBox({format: "#",decimals: 0});        
        $("#txtMemberQueryAge2").kendoNumericTextBox({format: "#",decimals: 0});        
        $("#txtMemberQuerySex").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sexQueryData,
                index: 0
        });  
        $("#txtMemberQueryPurpose").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: purposeQueryData,
                index: 0
        }); 
        
        ConditionOption = $("#conditionOption").kendoMultiSelect().data("kendoMultiSelect");
        
        $("#btnMemberQuery").kendoButton({
            click: function onClick(e) { 
                var result = validatorMemberQueryFrom();                
                if(result){
                	var url = makeQueryUrl(); 
                	memberQueryUrl = url;
                	showMemberCenterWindowsQuery(url);
                }                
            }
        });
    }    
}

function showMemberQueryWindows(){
    
    if(optMemberQueryWindows == null){
        
    	createMemberQueryWindows();
    }
    
    optMemberQueryWindows.data("kendoWindow").open();
}

function closeMemberQueryWindows(){
    
    if(optMemberQueryWindows != null){
        
    	optMemberQueryWindows.data("kendoWindow").close();
    }
}

function validatorMemberQueryFrom(){
    
    var result = true;
        
    return result;
}

function makeQueryUrl(){
	
	var txtKeyWord = $("#txtMemberQueryWord").val().trim();	
	var age1 = $("#txtMemberQueryAge1").data("kendoNumericTextBox").value();
	var age2 = $("#txtMemberQueryAge2").data("kendoNumericTextBox").value();
	var sex = $("#txtMemberQuerySex").data("kendoDropDownList").value();
	var plan = $("#txtMemberQueryPurpose").data("kendoDropDownList").value();
	
	var opt_fans ="";
	var opt_focuser = "";
	var opt_online = "";
	var opt_contact = "";
	var opt = ',,,' + ConditionOption.value() + ',,,';
	if( opt.indexOf(",1,") > 1) opt_online="T";
	if( opt.indexOf(",2,") > 1) opt_contact="T";
	if( opt.indexOf(",3,") > 1) opt_fans="T";
	if( opt.indexOf(",4,") > 1) opt_focuser="T";
    
	var url = "member/getMemberCenterList.json" + extendUrl02()
		+ "&USER_PTR="
		+ "&KEY_WORDS=" + encode2(txtKeyWord)
		+ "&QUERY_SEX=" + sex
		+ "&QUERY_PLAN=" + plan
		+ "&QUERY_AGE1=" + age1
		+ "&QUERY_AGE2=" + age2
		+ "&OPT_FANS=" + opt_fans
		+ "&OPT_FOCUSER=" + opt_focuser
		+ "&OPT_ONLINE=" + opt_online
		+ "&OPT_HAVE_CONTACT=" + opt_contact;	
	
	return url;
}

//会员中心窗口
function createMemberCenterWindows(){
    
    if(optMemberCenterWidows == null){        
    	optMemberCenterWidows = $("#memberCenterWindowsDiv").kendoWindow({            
            width: "800px",
            height: "560px",
            title: "会员中心",
            resizable: true,
            actions: ["Query", "Refresh", "Close"],
            refresh: function onRefresh(e) {                 	
            	showMemberCenterWindowsQuery(memberQueryUrl);
        	}            
        });  
        
        $("#memberCenterWindowsDiv").data("kendoWindow").wrapper.find(".k-i-query").click(function(e){    		
            e.preventDefault();
            showMemberQueryWindows();
        });
        
    }     
}

function showMemberCenterWindowsQuery(url){
    
    if(optMemberCenterWidows == null){
        
    	createMemberCenterWindows();
    }
    
    optMemberCenterWidows.data("kendoWindow").open();
    
    bindMemberCenterView(url);
}

function bindMemberCenterView(baseUrl){
    
    var dataSource = new kendo.data.DataSource({
    	transport: {
            read: {
                url: baseUrl,
                dataType: "json"
            }
        },
        requestStart: function() {
            $("#memberCenterWindowsLoading").show();
        },
        requestEnd: function() {
        	$("#memberCenterWindowsLoading").hide();
        },
        pageSize: 8
    });
    
    $("#memberCenterPager").kendoPager({
        dataSource: dataSource
    });
    
    $("#memberCenterListView").kendoListView({
        dataSource: dataSource,
        template: kendo.template($("#memberCenterTemplate").html())
    });
}

function closeMemberCenterWindows(){
    
    if(optMemberCenterWidows != null){
        
    	optMemberCenterWidows.data("kendoWindow").close();
    }
}


//会员个人窗口
function createMemberWindows(){
    
    if(optMemberWidows == null){        
    	optMemberWidows = $("#memberWindowsDiv").kendoWindow({            
            width: "800px",
            height: "590px",
            title: "会员资料",
            resizable: false,
            actions: ["Refresh", "Close"],
            refresh: function onRefresh(e) {             	
            	if(currentMemberUserPtr != -1){
	                bindMemberView(currentMemberUserPtr);                
	                bindMemberPhotoView(currentMemberUserPtr);
            	}                
        	}            
        });  
        
    }     
}

function showMemberWindowsQuery(userPtr){
    
    if(optMemberWidows == null){
        
    	createMemberWindows();
    }
    
    optMemberWidows.data("kendoWindow").open();
    
    bindMemberView(userPtr);
    
    bindMemberPhotoView(userPtr);
    
    currentMemberUserPtr = userPtr;
}

function bindMemberView(userPtr){
    
	var baseUrl = "member/getMemberInfo.json" + extendUrl02()
			+ "&USER_PTR=" + userPtr;
	
    var dataSource = new kendo.data.DataSource({
    	transport: {
            read: {
                url: baseUrl,
                dataType: "json"
            }
        },
        requestStart: function() {
            $("#memberWindowsLoading").show();
        },
        requestEnd: function() {
        	$("#memberWindowsLoading").hide();
        },
        pageSize: 1
    });
    
    $("#memberInfoView").kendoListView({
        dataSource: dataSource,
        template: kendo.template($("#memberTemplate").html())
    });
}


function bindMemberPhotoView(userPtr){
    
	var baseUrl = "user/getUserPhotoListInfo.json" + extendUrl02() + "&Pkey=" + userPtr; 
	
    var dataSource = new kendo.data.DataSource({
    	transport: {
            read: {
                url: baseUrl,
                dataType: "json"
            }
        },
        requestStart: function() {
            $("#memberWindowsLoading").show();
        },
        requestEnd: function() {
        	$("#memberWindowsLoading").hide();
        },
        pageSize: 4
    });
    
    $("#memberPhotoView").kendoListView({
        dataSource: dataSource,
        template: kendo.template($("#memberPhotoTemplate").html())
    }); 
    
    $("#memberPhotoPager").kendoPager({
        dataSource: dataSource
    });
}

function closeMemberWindows(){
    
    if(optMemberWidows != null){
        
    	optMemberWidows.data("kendoWindow").close();
    }
    
}

//About me 窗口
function createAboutMeWindows(){
    
    if(optAboutMeWindows == null){        
    	optAboutMeWindows = $("#aboutMeWindowsDiv").kendoWindow({            
            width: "600px",
            height: "320px",
            title: "关于我们",
            resizable: false,
            actions: ["Close"]           
        });         
    }     
}

function showAboutMeWindows(){
    
    if(optAboutMeWindows == null){
        
    	createAboutMeWindows();
    }
    
    optAboutMeWindows.data("kendoWindow").open();
}

function closeAboutMeWindows(){
    
    if(optAboutMeWindows != null){
        
    	optAboutMeWindows.data("kendoWindow").close();
    }
}

//trun on fans
function beFansTipsChange(self){
	
	var fansTips =  $(self).html(); 	

	if('关注对方'==fansTips){		
		$(self).html('取消关注');
		//$(self).parent().next().html( '<label class="displayFocuserT">我的粉丝</label>');
	}else if('取消关注'==fansTips){
		$(self).html('关注对方');
		//$(self).parent().next().html( '<label class="displayFocuserF">我的粉丝</label>');
	}else{
		$(self).html('');
		//$(self).parent().next().html( '<label class="displayFocuserF">我的粉丝</label>');
	}
}

function beFans(self, fansPtr){	
	
	$('#memberWindowsLoading').show();
	
    $.ajax({ 
	  	url: "member/trunOnFans.json" + extendUrl02() + "&FansPtr=" + fansPtr, 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){
  				
  				if(json.result=='SUCCESS'){  					
  					
  					beFansTipsChange(self);
  					
  				}else{  					
                    $("#memberWindowsValidator").text(json.context)
	                    .removeClass("valid")
	                	.addClass("invalid"); 
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {
            $("#memberWindowsValidator").text("操作失败")
	            .removeClass("valid")
	        	.addClass("invalid"); 
        },
	  	complete: function(x) {
	  		$('#memberWindowsLoading').hide();
	  	} 
	});	
}


//弹出对话面板 User talk about pannael
function createTalkPannelWindows(){
    
    if(optTalkPannelWindows == null){        
    	optTalkPannelWindows = $("#talkPannelWindowsDiv").kendoWindow({            
            width: "240px",
            height: "340px",
            title: "消息面板",
            resizable: false,
            actions: ["Refresh", "Close"],
            refresh: function onRefresh(e) {             	
            	bindTalkPannelView();
        	},
        	close: function onRefresh(e) {             	
        		stop_talkPannelTimer();
        	}
        });  
        
    }     
}

function showTalkPannelWindows(){
    
    if(optTalkPannelWindows == null){
        
    	createTalkPannelWindows();
    }
    
    optTalkPannelWindows.data("kendoWindow").open();
    
    bindTalkPannelView();
    
    run_talkPannelTimer();
    
}

function bindTalkPannelView(){
    
	var baseUrl = "talk/getTalkPannelInfo.json" + extendUrl02();
	
    var dataSource = new kendo.data.DataSource({
    	transport: {
            read: {
                url: baseUrl,
                dataType: "json"
            }
        }//,
        //requestStart: function() {
            //$("#talkPannelWindowsLoading").show();
        //},
        //requestEnd: function() {
        	//$("#talkPannelWindowsLoading").hide();
        //}
    });
    
    $("#talkPannelView").kendoListView({
        dataSource: dataSource,
        template: kendo.template($("#talkPannelTemplate").html())
    });
}

function closeTalkPannelWindows(){   
	
    if(optTalkPannelWindows != null){
    	
    	optTalkPannelWindows.data("kendoWindow").close();
    	
    }    
}


//弹出对话窗口
function showTalkAbout(receiveUserPtr, receiveUserName){
	
	clearTalkAboutWindows();
	
	setValueTalkAboutWindows(receiveUserPtr, receiveUserName);
	
	showTalkAboutWindows();
	
}

function createTalkAboutWindows(){
    
    if(optTalkAboutWindows == null){   
    	
    	optTalkAboutWindows = $("#talkWindowsDiv").kendoWindow({            
            width: "420px",
            height: "320px",
            title: "发送消息",
            resizable: false,
            actions: ["Close"]          
        });   
    	
        $("#btnSendMessage").kendoButton({        	
            click: function onClick(e) {  
            	
            	//验证控件内容
            	var msg = validatorTalkAboutWindows();
            	
            	if(msg==""){            		
            		doneTalkAbout();
            	}else{
            		
            	    $("#talkValidator").text(msg)
	            	    .removeClass("valid")
	            		.addClass("invalid"); 
            	}
            	
            }
        });
        
    }     
}

function showTalkAboutWindows(){
	
    if(optTalkAboutWindows == null){    	
    	
    	createTalkAboutWindows();    	
    } 
    
    optTalkAboutWindows.data("kendoWindow").open();
}

function closeTalkAboutWindows(){
	
    if(optTalkAboutWindows != null){    	
    	
    	optTalkAboutWindows.data("kendoWindow").close();    	
    }   
    
    clearTalkAboutWindows();
}

function clearTalkAboutWindows(){
	
	$("#labTalkReceiveUserName").text("");
	
	$("#labTalkReceiveUserPtr").text("");
	
	$("#txtTalkAbout").val("");
	
	talkerPtr = -1;
	
	talkerName = "";
	
    $("#talkValidator").text("")
	    .removeClass("invalid")
		.addClass("valid"); 
}

function setValueTalkAboutWindows(receiveUserPtr, receiveUserName){
	
	$("#labTalkReceiveUserName").text(receiveUserName);
	
	$("#labTalkReceiveUserPtr").text(receiveUserPtr);	
	
	$("#labTalkReceiveUserPtr").hide();
	
	talkerPtr = receiveUserPtr;
	
	talkerName = receiveUserName;
}

function validatorTalkAboutWindows(){
	
	var result = "";
	
	var userPtr = $("#labTalkReceiveUserPtr").text();
	var talkAbout = $("#txtTalkAbout").val();
	
	if(userPtr.length==0){
		
		return "亲，您这消息要发给谁啊";
	}
	
	if(talkAbout.length==0){
		
		return "亲，您想和对方说什么呢？这内容不能留空";
		
	}else if(talkAbout.length > 140){
		
		return "亲，消息的内容超过了 140 个字";
	}
	
	return result;
	
}

function getSendMessageUrl(){	
	
	var userPtr = $("#labTalkReceiveUserPtr").text();
	
	var talkAbout = $("#txtTalkAbout").val();
	
	return "&RecevieUserPtr=" + userPtr + "&TalkAbout=" + encode2(talkAbout);
}

function doneTalkAbout(){
	
	$('#talkWindowsLoading').show();
	
    $.ajax({ 
	  	url: "talk/sendMessage.json" + extendUrl02() + getSendMessageUrl(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){
  				
  				if(json.result=='SUCCESS'){  					
  					
  					$("#txtTalkAbout").val("");
  					
                    $("#talkValidator").text(json.context)
	                    .removeClass("invalid")
	                	.addClass("valid"); 
  					
  				}else{  					
                    $("#talkValidator").text(json.context)
	                    .removeClass("valid")
	                	.addClass("invalid"); 
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {
	  		
            $("#talkValidator").text("操作失败")
	            .removeClass("valid")
	        	.addClass("invalid"); 
        },
	  	complete: function(x) {
	  		
	  		$('#talkWindowsLoading').hide();
	  	} 
	});	
    
}

//判断菜单的第三列是否为“消息”
function checkMenuItemTalkExist(){
	
	var result = false;
	
	var objTalkMenuItem=$('#menuUL li').next().next().children('span:first');
	
	if(objTalkMenuItem.indexOf("消息") > -1){
		
		result = true;
	}
	
	return result;
	
}

function setMenuItemTalkTips(count){
	
	var objTalkMenuItem=$('#menuUL li').next().next().children('span:first');
	
	var menuItemTitle = objTalkMenuItem.html();
	
	if(menuItemTitle.indexOf("消息") > -1){
		
		if(count > 0 ){
			objTalkMenuItem.html( "消息(" + count + ")<span class=\"k-icon k-i-arrow-s\"></span>");
		}else{
			objTalkMenuItem.html( "消息<span class=\"k-icon k-i-arrow-s\"></span>");
		}		
		
	}else{
		
		stop_talkTipsTimer();
	}
	
}


function run_talkTipsTimer(){
	
	if(talkTipsTimer==null){
		
		talkTipsTimer = $.timer(5000, function() {

		    $.ajax({ 
			  	url: "talk/getUserTalkTips.json" + extendUrl02(), 
			  	type: "POST", 
			  	dataType: "json",
			  	contentType: "application/json; charset=utf-8", 
			  	data: "{}",
			  	global: true,
			  	success: function(json){	  		
		  			if((json != null)&&(json!= "undefined")){
		  				
		  				if((json.result=='ERROR') || (json.result=="FAIL")){

		  					setMenuItemTalkTips("?");
		 					
		  				}else{				
		 
		  					setMenuItemTalkTips(json.context);		  					
		  				} 
		  				
		  			}
			  	}, 
			  	error: function(x, e) {
			  		//
		        },
			  	complete: function(x) {
			  		//
			  	} 
			});
		    
		});
	}
	
}

function stop_talkTipsTimer(){
	
	if(talkTipsTimer != null){
		
		talkTipsTimer.stop();
	}
	
	talkTipsTimer = null;
}


//聊天面板定时器 talkPannelTimer
function run_talkPannelTimer(){
	
	if(talkPannelTimer==null){
		
		talkPannelTimer = $.timer(5000, function() {
			
			bindTalkPannelView();
		    
		});
	}
	
}

function stop_talkPannelTimer(){
	
	if(talkPannelTimer != null){
		
		talkPannelTimer.stop();
	}
	
	talkPannelTimer = null;
}


//聊天记录窗口
function bindTalksListView(talker){
    
	talksListViewDS = new kendo.data.DataSource({
    	pageSize: 8,
        transport: {
            read: {
                url: "talk/getTalkAboutInfo.json" + extendUrl02() + "&Talker=" + talker, 
                dataType: "json"
            }
        }
    });
    
	//talksListViewDS.read();
	
	$("#talksListViewPager").kendoPager({
        dataSource: talksListViewDS
    });
    
    $("#talksListView").kendoListView({
        dataSource: talksListViewDS,
        template: kendo.template($("#talksListTemplate").html())
    });
     
}

function createTalksListWindows(){
    
    if(optTalksListWindows == null){   
    	
    	optTalksListWindows = $("#talkInfoWindowsDiv").kendoWindow({            
            width: "600px",
            height: "430px",
            title: "聊天记录",
            resizable: false,
            actions: ["Refresh", "Close"],
            refresh: function onRefresh(e) { 
            	
            	bindTalksListView(talkerPtr);
        	}         
        });  
    	        
    }     
}

function showTalksListWindows(talker){
	
	if(optTalksListWindows == null){
		
		createTalksListWindows();
	}
	
	optTalksListWindows.data("kendoWindow").open();
	
	talkerPtr = talker;
	
	bindTalksListView(talker);
}

function closeTalksListWindows(){
	
	if(optTalksListWindows != null){
		
		optTalksListWindows.data("kendoWindow").close(); 
	}
}


//未读取的聊天信息窗口
function createNewTalkInfoWindows(){
	
    if(optNewTalkInfoWindows == null){   
    	
    	optNewTalkInfoWindows = $("#newTalkInfoWindowsDiv").kendoWindow({       		
            width: "600px",
            height: "430px",
            title: "最近的新信息",
            resizable: false,
            actions: ["sendmsg", "Refresh", "Close"],
            refresh: function onRefresh(e) { 
            	
            	bindNewTalksListView(talkerPtr);
        	}         
        }); 
    	
        //Send Message button
        $("#newTalkInfoWindowsDiv").data("kendoWindow").wrapper.find(".k-i-sendmsg").click(function(e){
        	showTalkAbout(talkerPtr, talkerName);
            e.preventDefault();
        });
    	        
    }
}

function closeNewTalkInfoWindows(){
	
	if(optNewTalkInfoWindows != null){
		
		optNewTalkInfoWindows.data("kendoWindow").close(); 
	}
}

function showNewTalkInfoWindows(talker, name){
	
	if(optNewTalkInfoWindows == null){
		
		createNewTalkInfoWindows();
	}
	
	optNewTalkInfoWindows.data("kendoWindow").open();
	
	talkerPtr = talker;
	
	talkerName = name;
	
	bindNewTalksListView(talker);
}

//获取所有未读取的聊天信息
function bindNewTalksListView(talker){
    
	newTalksListViewDS = new kendo.data.DataSource({
        transport: {
            read: {
                url: "talk/getUserNewTalks.json" + extendUrl02() + "&Talker=" + talker, 
                dataType: "json"
            }
        }
    });
    
	//newTalksListViewDS.read();
    
    $("#newTalksListView").kendoListView({
        dataSource: newTalksListViewDS,
        template: kendo.template($("#newTalksListTemplate").html())
    });
     
}



//关闭全部窗口
function closeAllWindows(){

	closeMsgBoxWindows();
	closeLoginWindows();
	closeRegisterWindows();
	closeForgetWindows();
	closeBaseUserInfoWindows();
	closeBeFriendWindows();
	closeContactWindows();
	closePhotoWindows();
	closeUploadImageWindows();
	closeMemberQueryWindows();
	closeMemberWindows();
	closeMemberCenterWindows();
	
	closeAboutMeWindows();
	closeTalkPannelWindows();
	closeTalksListWindows();
	closeNewTalkInfoWindows();
}





