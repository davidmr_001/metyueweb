package metyue.model;

import java.util.Date;

public class UserPhotoListVO {

	private long imagePtr;
	private long groupPtr;
	private String groupName;
    private long userPtr;
    private String imageSrc;
    private String imageResizeSrc;
    private String imageTips;
    private String imageClass;
    private String displayDesc;
    private String coverDesc;
    private String displayFlg;
    private String coverFlg;
    private String checkStatus;
    private Date lastUpdate;
    
	public UserPhotoListVO() {
		super();
	}

	public UserPhotoListVO(long imagePtr, long groupPtr, String groupName,
			long userPtr, String imageSrc, String imageResizeSrc,
			String imageTips, String imageClass, String displayDesc,
			String coverDesc, String displayFlg, String coverFlg,
			String checkStatus, Date lastUpdate) {
		super();
		this.imagePtr = imagePtr;
		this.groupPtr = groupPtr;
		this.groupName = groupName;
		this.userPtr = userPtr;
		this.imageSrc = imageSrc;
		this.imageResizeSrc = imageResizeSrc;
		this.imageTips = imageTips;
		this.imageClass = imageClass;
		this.displayDesc = displayDesc;
		this.coverDesc = coverDesc;
		this.displayFlg = displayFlg;
		this.coverFlg = coverFlg;
		this.checkStatus = checkStatus;
		this.lastUpdate = lastUpdate;
	}

	public long getImagePtr() {
		return imagePtr;
	}

	public void setImagePtr(long imagePtr) {
		this.imagePtr = imagePtr;
	}

	public long getGroupPtr() {
		return groupPtr;
	}

	public void setGroupPtr(long groupPtr) {
		this.groupPtr = groupPtr;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public long getUserPtr() {
		return userPtr;
	}

	public void setUserPtr(long userPtr) {
		this.userPtr = userPtr;
	}

	public String getImageSrc() {
		return imageSrc;
	}

	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}

	public String getImageResizeSrc() {
		return imageResizeSrc;
	}

	public void setImageResizeSrc(String imageResizeSrc) {
		this.imageResizeSrc = imageResizeSrc;
	}

	public String getImageTips() {
		return imageTips;
	}

	public void setImageTips(String imageTips) {
		this.imageTips = imageTips;
	}

	public String getImageClass() {
		return imageClass;
	}

	public void setImageClass(String imageClass) {
		this.imageClass = imageClass;
	}

	public String getDisplayDesc() {
		return displayDesc;
	}

	public void setDisplayDesc(String displayDesc) {
		this.displayDesc = displayDesc;
	}

	public String getCoverDesc() {
		return coverDesc;
	}

	public void setCoverDesc(String coverDesc) {
		this.coverDesc = coverDesc;
	}

	public String getDisplayFlg() {
		return displayFlg;
	}

	public void setDisplayFlg(String displayFlg) {
		this.displayFlg = displayFlg;
	}

	public String getCoverFlg() {
		return coverFlg;
	}

	public void setCoverFlg(String coverFlg) {
		this.coverFlg = coverFlg;
	}

	public String getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	
}
